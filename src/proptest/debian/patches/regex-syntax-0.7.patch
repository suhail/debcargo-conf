This patch is based on the upstream commit described below, adapted for use in
the Debian package by Peter Michael Green.

commit f248d51f9fc6949b4798001d78d02fa292446c55
Author: Shaun Jackman <sjackman@gmail.com>
Date:   Wed Jul 12 18:11:13 2023 -0700

    build(deps): bump regex-syntax from 0.6 to 0.7 (#335)

Index: proptest/src/string.rs
===================================================================
--- proptest.orig/src/string.rs
+++ proptest/src/string.rs
@@ -16,13 +16,7 @@ use core::mem;
 use core::ops::RangeInclusive;
 use core::u32;
 
-use regex_syntax::hir::{
-    self, Hir,
-    HirKind::*,
-    Literal::*,
-    RepetitionKind::{self, *},
-    RepetitionRange::*,
-};
+use regex_syntax::hir::{self, Hir, HirKind::*, Repetition};
 use regex_syntax::{Error as ParseError, Parser};
 
 use crate::bool;
@@ -169,11 +163,7 @@ pub fn bytes_regex_parsed(expr: &Hir) ->
     match expr.kind() {
         Empty => Ok(Just(vec![]).sboxed()),
 
-        Literal(lit) => Ok(Just(match lit {
-            Unicode(scalar) => to_bytes(*scalar),
-            Byte(byte) => vec![*byte],
-        })
-        .sboxed()),
+        Literal(lit) => Ok(Just(lit.0.to_vec()).sboxed()),
 
         Class(class) => Ok(match class {
             hir::Class::Unicode(class) => {
@@ -185,19 +175,13 @@ pub fn bytes_regex_parsed(expr: &Hir) ->
             }
         }),
 
-        Repetition(rep) => Ok(vec(
-            bytes_regex_parsed(&rep.hir)?,
-            to_range(rep.kind.clone())?,
-        )
-        .prop_map(|parts| {
-            parts.into_iter().fold(vec![], |mut acc, child| {
-                acc.extend(child);
-                acc
-            })
-        })
-        .sboxed()),
+        Repetition(rep) => {
+            Ok(vec(bytes_regex_parsed(&rep.sub)?, to_range(rep)?)
+                .prop_map(|parts| parts.concat())
+                .sboxed())
+        }
 
-        Group(group) => bytes_regex_parsed(&group.hir).map(|v| v.0),
+        Capture(capture) => bytes_regex_parsed(&capture.sub).map(|v| v.0),
 
         Concat(subs) => {
             let subs = ConcatIter {
@@ -225,12 +209,8 @@ pub fn bytes_regex_parsed(expr: &Hir) ->
             Ok(Union::try_new(subs.iter().map(bytes_regex_parsed))?.sboxed())
         }
 
-        Anchor(_) => {
-            unsupported("line/text anchors not supported for string generation")
-        }
-
-        WordBoundary(_) => unsupported(
-            "word boundary tests not supported for string generation",
+        Look(_) => unsupported(
+            "anchors/boundaries not supported for string generation",
         ),
     }
     .map(RegexGeneratorStrategy)
@@ -291,8 +271,7 @@ impl<'a, I: Iterator<Item = &'a Hir>> It
         while let Some(next) = self.iter.next() {
             match next.kind() {
                 // A literal. Accumulate:
-                Literal(Unicode(scalar)) => self.buf.extend(to_bytes(*scalar)),
-                Literal(Byte(byte)) => self.buf.push(*byte),
+                Literal(literal) => self.buf.extend_from_slice(&literal.0),
                 // Encountered a non-literal.
                 _ => {
                     return if !self.buf.is_empty() {
@@ -317,31 +296,35 @@ impl<'a, I: Iterator<Item = &'a Hir>> It
     }
 }
 
-fn to_range(kind: RepetitionKind) -> Result<SizeRange, Error> {
-    Ok(match kind {
-        ZeroOrOne => size_range(0..=1),
-        ZeroOrMore => size_range(0..=32),
-        OneOrMore => size_range(1..=32),
-        Range(range) => match range {
-            Exactly(count) if u32::MAX == count => {
-                return unsupported(
-                    "Cannot have repetition of exactly u32::MAX",
-                )
-            }
-            Exactly(count) => size_range(count as usize),
-            AtLeast(min) => {
-                let max = if min < u32::MAX as u32 / 2 {
-                    min as usize * 2
-                } else {
-                    u32::MAX as usize
-                };
-                size_range((min as usize)..max)
-            }
-            Bounded(_, max) if u32::MAX == max => {
-                return unsupported("Cannot have repetition max of u32::MAX")
-            }
-            Bounded(min, max) => size_range((min as usize)..(max as usize + 1)),
-        },
+fn to_range(rep: &Repetition) -> Result<SizeRange, Error> {
+    Ok(match (rep.min, rep.max) {
+        // Zero or one
+        (0, Some(1)) => size_range(0..=1),
+        // Zero or more
+        (0, None) => size_range(0..=32),
+        // One or more
+        (1, None) => size_range(1..=32),
+        // Exact count of u32::MAX
+        (u32::MAX, Some(u32::MAX)) => {
+            return unsupported("Cannot have repetition of exactly u32::MAX");
+        }
+        // Exact count
+        (min, Some(max)) if min == max => size_range(min as usize),
+        // At least min
+        (min, None) => {
+            let max = if min < u32::MAX as u32 / 2 {
+                min as usize * 2
+            } else {
+                u32::MAX as usize
+            };
+            size_range((min as usize)..max)
+        }
+        // Bounded range with max of u32::MAX
+        (_, Some(u32::MAX)) => {
+            return unsupported("Cannot have repetition max of u32::MAX")
+        }
+        // Bounded range
+        (min, Some(max)) => size_range((min as usize)..(max as usize + 1)),
     })
 }
 
Index: proptest/Cargo.toml
===================================================================
--- proptest.orig/Cargo.toml
+++ proptest/Cargo.toml
@@ -60,7 +60,7 @@ default-features = false
 version = "0.3"
 
 [dependencies.regex-syntax]
-version = "0.6.0"
+version = "0.7"
 optional = true
 
 [dependencies.rusty-fork]
@@ -73,7 +73,7 @@ version = "3.0"
 optional = true
 
 [dev-dependencies.regex]
-version = "1.0"
+version = "1.8.0"
 
 [features]
 alloc = []
